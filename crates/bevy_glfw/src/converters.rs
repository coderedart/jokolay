use bevy_input::{keyboard::KeyCode, mouse::MouseButton, ElementState};
use bevy_utils::tracing::error;

use bevy_window::CursorIcon;
use glfw::{Action, Key};

// pub fn convert_keyboard_input(keyboard_input: &winit::event::KeyboardInput) -> KeyboardInput {
//     KeyboardInput {
//         scan_code: keyboard_input.scancode,
//         state: convert_element_state(keyboard_input.state),
//         key_code: keyboard_input.virtual_keycode.map(convert_virtual_key_code),
//     }
// }

pub fn convert_element_state(element_state: Action) -> ElementState {
    match element_state {
        Action::Press => ElementState::Pressed,
        Action::Release => ElementState::Released,
        Action::Repeat => ElementState::Pressed,
    }
}

pub fn convert_mouse_button(mouse_button: glfw::MouseButton) -> MouseButton {
    match mouse_button {
        glfw::MouseButton::Button1 => MouseButton::Left,
        glfw::MouseButton::Button2 => MouseButton::Right,
        glfw::MouseButton::Button3 => MouseButton::Middle,
        glfw::MouseButton::Button4 => MouseButton::Other(4),
        glfw::MouseButton::Button5 => MouseButton::Other(5),
        glfw::MouseButton::Button6 => MouseButton::Other(6),
        glfw::MouseButton::Button7 => MouseButton::Other(7),
        glfw::MouseButton::Button8 => MouseButton::Other(8),
    }
}

pub fn convert_virtual_key_code(virtual_key_code: Key) -> KeyCode {
    match virtual_key_code {
        Key::Num1 => KeyCode::Key1,
        Key::Num2 => KeyCode::Key2,
        Key::Num3 => KeyCode::Key3,
        Key::Num4 => KeyCode::Key4,
        Key::Num5 => KeyCode::Key5,
        Key::Num6 => KeyCode::Key6,
        Key::Num7 => KeyCode::Key7,
        Key::Num8 => KeyCode::Key8,
        Key::Num9 => KeyCode::Key9,
        Key::Num0 => KeyCode::Key0,
        Key::A => KeyCode::A,
        Key::B => KeyCode::B,
        Key::C => KeyCode::C,
        Key::D => KeyCode::D,
        Key::E => KeyCode::E,
        Key::F => KeyCode::F,
        Key::G => KeyCode::G,
        Key::H => KeyCode::H,
        Key::I => KeyCode::I,
        Key::J => KeyCode::J,
        Key::K => KeyCode::K,
        Key::L => KeyCode::L,
        Key::M => KeyCode::M,
        Key::N => KeyCode::N,
        Key::O => KeyCode::O,
        Key::P => KeyCode::P,
        Key::Q => KeyCode::Q,
        Key::R => KeyCode::R,
        Key::S => KeyCode::S,
        Key::T => KeyCode::T,
        Key::U => KeyCode::U,
        Key::V => KeyCode::V,
        Key::W => KeyCode::W,
        Key::X => KeyCode::X,
        Key::Y => KeyCode::Y,
        Key::Z => KeyCode::Z,
        Key::Escape => KeyCode::Escape,
        Key::F1 => KeyCode::F1,
        Key::F2 => KeyCode::F2,
        Key::F3 => KeyCode::F3,
        Key::F4 => KeyCode::F4,
        Key::F5 => KeyCode::F5,
        Key::F6 => KeyCode::F6,
        Key::F7 => KeyCode::F7,
        Key::F8 => KeyCode::F8,
        Key::F9 => KeyCode::F9,
        Key::F10 => KeyCode::F10,
        Key::F11 => KeyCode::F11,
        Key::F12 => KeyCode::F12,
        Key::F13 => KeyCode::F13,
        Key::F14 => KeyCode::F14,
        Key::F15 => KeyCode::F15,
        Key::F16 => KeyCode::F16,
        Key::F17 => KeyCode::F17,
        Key::F18 => KeyCode::F18,
        Key::F19 => KeyCode::F19,
        Key::F20 => KeyCode::F20,
        Key::F21 => KeyCode::F21,
        Key::F22 => KeyCode::F22,
        Key::F23 => KeyCode::F23,
        Key::F24 => KeyCode::F24,
        Key::PrintScreen => KeyCode::Snapshot,
        Key::ScrollLock => KeyCode::Scroll,
        Key::Pause => KeyCode::Pause,
        Key::Insert => KeyCode::Insert,
        Key::Home => KeyCode::Home,
        Key::Delete => KeyCode::Delete,
        Key::End => KeyCode::End,
        Key::PageDown => KeyCode::PageDown,
        Key::PageUp => KeyCode::PageUp,
        Key::Left => KeyCode::Left,
        Key::Up => KeyCode::Up,
        Key::Right => KeyCode::Right,
        Key::Down => KeyCode::Down,
        Key::Backspace => KeyCode::Back,
        Key::Enter => KeyCode::Return,
        Key::Space => KeyCode::Space,
        Key::NumLock => KeyCode::Numlock,
        Key::Kp0 => KeyCode::Numpad0,
        Key::Kp1 => KeyCode::Numpad1,
        Key::Kp2 => KeyCode::Numpad2,
        Key::Kp3 => KeyCode::Numpad3,
        Key::Kp4 => KeyCode::Numpad4,
        Key::Kp5 => KeyCode::Numpad5,
        Key::Kp6 => KeyCode::Numpad6,
        Key::Kp7 => KeyCode::Numpad7,
        Key::Kp8 => KeyCode::Numpad8,
        Key::Kp9 => KeyCode::Numpad9,
        Key::KpAdd => KeyCode::NumpadAdd,
        Key::Apostrophe => KeyCode::Apostrophe,
        Key::Backslash => KeyCode::Backslash,
        Key::Comma => KeyCode::Comma,
        Key::KpDecimal => KeyCode::NumpadDecimal,
        Key::KpDivide => KeyCode::NumpadDivide,
        Key::GraveAccent => KeyCode::Grave,
        Key::LeftAlt => KeyCode::LAlt,
        Key::LeftBracket => KeyCode::LBracket,
        Key::LeftControl => KeyCode::LControl,
        Key::LeftShift => KeyCode::LShift,
        Key::LeftSuper => KeyCode::LWin,
        Key::Minus => KeyCode::Minus,
        Key::KpMultiply => KeyCode::NumpadMultiply,
        Key::KpEnter => KeyCode::NumpadEnter,
        Key::KpEqual => KeyCode::NumpadEquals,
        Key::Period => KeyCode::Period,
        Key::RightAlt => KeyCode::RAlt,
        Key::RightBracket => KeyCode::RBracket,
        Key::RightControl => KeyCode::RControl,
        Key::RightShift => KeyCode::RShift,
        Key::RightSuper => KeyCode::RWin,
        Key::Semicolon => KeyCode::Semicolon,
        Key::Slash => KeyCode::Slash,
        Key::KpSubtract => KeyCode::NumpadSubtract,
        Key::Tab => KeyCode::Tab,
        Key::Equal => KeyCode::Equals,
        Key::CapsLock => KeyCode::Capital,
        rest => {
            error!("no valid key to convert to: {rest:?}");
            KeyCode::Unlabeled
        }
    }
}

pub fn convert_cursor_icon(cursor_icon: CursorIcon) -> glfw::StandardCursor {
    match cursor_icon {
        CursorIcon::Default => glfw::StandardCursor::Arrow,
        CursorIcon::Crosshair => glfw::StandardCursor::Crosshair,
        CursorIcon::Hand => glfw::StandardCursor::Hand,
        CursorIcon::Arrow => glfw::StandardCursor::Arrow,
        CursorIcon::EResize => glfw::StandardCursor::HResize,
        CursorIcon::NResize => glfw::StandardCursor::VResize,
        CursorIcon::SResize => glfw::StandardCursor::VResize,
        CursorIcon::WResize => glfw::StandardCursor::HResize,
        CursorIcon::EwResize => glfw::StandardCursor::HResize,
        CursorIcon::NsResize => glfw::StandardCursor::VResize,
        _ => glfw::StandardCursor::Arrow,
    }
}
